package com.mytechjob.service.impl;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.mytechjob.repository.model.entity.Country;
import com.mytechjob.repository.model.entity.PhoneNumber;
import com.mytechjob.repository.persistence.dao.PhoneNumberDAO;
import com.mytechjob.service.PhoneNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PhoneNumberServiceImpl implements PhoneNumberService{

    @Autowired
    PhoneNumberDAO phoneNumberDAO;

    @Override
    public PhoneNumber validateAndCreatePhoneNumber(String number, Country country) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(number,country.getCountryCodeA2());
            if(!phoneNumberUtil.isPossibleNumber(phoneNumber) || !phoneNumberUtil.isValidNumber(phoneNumber)){
                return null;
            }
            PhoneNumber result = new PhoneNumber();
            result.setE164(phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164));
            result.setInternationalFormat(phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL));
            result.setNationalFormat(phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.NATIONAL));
            return result;
        } catch (NumberParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public PhoneNumber findOneById(long id) {
        return phoneNumberDAO.findOneById(id);
    }

    @Override
    public void remove(PhoneNumber number) {
        phoneNumberDAO.remove(number);
    }

    @Override
    public boolean checkForExist(PhoneNumber number) {
        return phoneNumberDAO.checkForExist(number);
    }
}
