<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $(".address_info").click(function (event) {
            event.preventDefault();
            var id = $(this).data("id");
            window.location.replace("/address/" + id);
        })

        $("#address_add").click(function (event) {
            event.preventDefault();
            $("#address_popup").bPopup({
                onClose:function(){
                    $("#country_name").val('default');
                }
            });
        })

        $("#address_submit").click(function (event) {
            event.preventDefault();
            var country = $("#country_name").val();
            if(country == "default"){
                alert("Please select country ...");
                return;
            }
            var data = {
                "country": country
            }
            $.ajax({
                url: '/address',
                data: JSON.stringify(data),
                contentType: "application/json",
                type: 'POST',
                success: function(data){
                    console.log(data);
                    if(data == "address already exist"){
                        alert("Address already exist ...")
                    }else{
                        window.location.reload();
                    }
                }
            })
        })
    });
</script>
