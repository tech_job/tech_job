package com.mytechjob.service;

import com.mytechjob.repository.model.entity.Country;
import com.mytechjob.repository.model.entity.PhoneNumber;

public interface PhoneNumberService {
    PhoneNumber findOneById(long id);
    PhoneNumber validateAndCreatePhoneNumber(String number,Country country);
    void remove(PhoneNumber number);
    boolean checkForExist(PhoneNumber number);
}
