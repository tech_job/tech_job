package com.mytechjob.repository.persistence.dao;

import com.mytechjob.repository.model.entity.Address;
import com.mytechjob.repository.model.entity.Country;

import java.util.List;

public interface AddressDAO {
    Address findOneById(long id);
    Address findOneByCountry(Country country);
    void createOrUpdate(Address address);
    void remove(Address address);
    List<Address> getAll();
}
