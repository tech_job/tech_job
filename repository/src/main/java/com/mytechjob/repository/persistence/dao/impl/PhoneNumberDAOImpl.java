package com.mytechjob.repository.persistence.dao.impl;

import com.mytechjob.repository.model.entity.PhoneNumber;
import com.mytechjob.repository.persistence.dao.PhoneNumberDAO;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public class PhoneNumberDAOImpl implements PhoneNumberDAO{

    @Autowired
    HibernateTemplate hibernateTemplate;

    @Override
    public PhoneNumber findOneById(long id) {
        return hibernateTemplate.get(PhoneNumber.class,id);
    }

    @Override
    @Transactional
    public void createOrUpdate(PhoneNumber phoneNumber) {
        hibernateTemplate.saveOrUpdate(phoneNumber);
    }

    @Override
    @Transactional
    public void remove(PhoneNumber phoneNumber) {
        hibernateTemplate.delete(phoneNumber);
    }

    @Override
    public List<PhoneNumber> getAll() {
        return (List<PhoneNumber>) hibernateTemplate.find("from PhoneNumber");
    }

    @Override
    public boolean checkForExist(PhoneNumber number) {
        DetachedCriteria criteria = DetachedCriteria.forClass(PhoneNumber.class);
        criteria.add(Restrictions.eq("e164",number.getE164()));
        List<PhoneNumber> result = (List<PhoneNumber>) hibernateTemplate.findByCriteria(criteria);
        return result.size() > 0;
    }
}
