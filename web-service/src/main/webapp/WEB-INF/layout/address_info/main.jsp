<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <input id="enum_name" type="hidden" value="${country}">
    <div>
        Country name: ${country.countryName}
    </div>
    <div>
        ISO2: ${country.countryCodeA2}
    </div>
    <div>
        ISO3: ${country.countryCodeA3}
    </div>
    <div>
        <table border="1">
            <tr>
                <th>
                    ID
                </th>
                <th>
                    NATIONAL FORMAT
                </th>
                <th>
                    INTERNATIONAL FORMAT
                </th>
                <th>
                    E164
                </th>
                <th>

                </th>
            </tr>
            <c:choose>
                <c:when test="${not empty numbers}">
                    <c:forEach var="entry" items="${numbers}">
                        <tr>
                            <td>${entry.phoneNumberId}</td>
                            <td>${entry.nationalFormat}</td>
                            <td>${entry.internationalFormat}</td>
                            <td>${entry.e164}</td>
                            <td><button class="number_remove" data-number-id="${entry.phoneNumberId}" data-address-id="${addressId}">Remove</button></td>
                        </tr>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <tr>
                        <td colspan="5">empty</td>
                    </tr>
                </c:otherwise>
            </c:choose>
        </table>
    </div>
    <div>
        <button id="number_add">Add number</button>
        <button id="back">Back to addresses</button>
    </div>
    <div id="number_popup" style="display: none; background-color: white; padding: 10px">
        <div>
            Number: <input id="number_input" type="text" placeholder="Enter number here ..." size="20">
        </div>
        <div>
            <button id="number_submit">Submit</button>
        </div>
    </div>
