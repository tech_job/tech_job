package com.mytechjob.repository.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "phone_number")
public class PhoneNumber {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "phone_number_id", unique = true)
    private long phoneNumberId;

    @Column(name = "e164")
    private String e164;

    @Column(name = "national")
    private String nationalFormat;

    @Column(name = "international")
    private String internationalFormat;

    public PhoneNumber() {
    }

    public long getPhoneNumberId() {
        return phoneNumberId;
    }

    public void setPhoneNumberId(long phoneNumberId) {
        this.phoneNumberId = phoneNumberId;
    }

    public String getE164() {
        return e164;
    }

    public void setE164(String e164) {
        this.e164 = e164;
    }

    public String getNationalFormat() {
        return nationalFormat;
    }

    public void setNationalFormat(String nationalFormat) {
        this.nationalFormat = nationalFormat;
    }

    public String getInternationalFormat() {
        return internationalFormat;
    }

    public void setInternationalFormat(String internationalFormat) {
        this.internationalFormat = internationalFormat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PhoneNumber that = (PhoneNumber) o;

        if (!e164.equals(that.e164)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (phoneNumberId ^ (phoneNumberId >>> 32));
    }
}
