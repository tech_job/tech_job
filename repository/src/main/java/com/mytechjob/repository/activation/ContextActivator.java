package com.mytechjob.repository.activation;

import com.mytechjob.repository.model.entity.Address;
import com.mytechjob.repository.model.entity.Country;
import com.mytechjob.repository.model.entity.PhoneNumber;
import com.mytechjob.repository.persistence.dao.AddressDAO;
import com.mytechjob.repository.persistence.dao.PhoneNumberDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

public class ContextActivator implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    PhoneNumberDAO phoneNumberDAO;
    @Autowired
    AddressDAO addressDAO;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        setAddresses();
    }

    private void setAddresses(){
        PhoneNumber phoneNumber1 = new PhoneNumber();
        phoneNumber1.setNationalFormat("096 123 4567");
        phoneNumber1.setInternationalFormat("+380 96 123 4567");
        phoneNumber1.setE164("+380961234567");

        PhoneNumber phoneNumber2 = new PhoneNumber();
        phoneNumber2.setNationalFormat("096 765 4321");
        phoneNumber2.setInternationalFormat("+380 96 765 4321");
        phoneNumber2.setE164("+380967654321");

        PhoneNumber phoneNumber3 = new PhoneNumber();
        phoneNumber3.setNationalFormat("070 112 3456");
        phoneNumber3.setInternationalFormat("+93 70 112 3456");
        phoneNumber3.setE164("+93701123456");

        PhoneNumber phoneNumber4 = new PhoneNumber();
        phoneNumber4.setNationalFormat("070 165 4321");
        phoneNumber4.setInternationalFormat("+93 70 165 4321");
        phoneNumber4.setE164("+93701654321");

        Address address1 = new Address();
        address1.setCountry(Country.AFGHANISTAN);
        address1.getNumbers().add(phoneNumber3);
        address1.getNumbers().add(phoneNumber4);

        Address address2 = new Address();
        address2.setCountry(Country.UKRAINE);
        address2.getNumbers().add(phoneNumber1);
        address2.getNumbers().add(phoneNumber2);

        addressDAO.createOrUpdate(address1);
        addressDAO.createOrUpdate(address2);
    }
}
