<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $(".number_remove").click(function (event) {
            event.preventDefault();
            var button = $(this);
            var addressId = button.data("address-id");
            var numberId = button.data("number-id");
            $.ajax({
                url: '/number?address=' + addressId + "&number=" + numberId,
                type: "GET",
                success:function(data){
                    console.log(data);
                    window.location.reload();
                }
            })
        });

        $("#number_add").click(function (event) {
            event.preventDefault();
            $("#number_popup").bPopup({
                onClose:function(){
                    $("#number_input").val('');
                }
            });
        });

        $("#number_submit").click(function (event) {
            event.preventDefault();
            var data={
                "phoneNumber":$("#number_input").val(),
                "country":$("#enum_name").val()
            };
            $.ajax({
                url: '/number',
                data: JSON.stringify(data),
                contentType: "application/json",
                type: 'POST',
                success: function(data){
                    console.log(data);
                    if(data == "not valid number"){
                        alert("Please enter valid phone number ...")
                    }else if(data == "number already exist"){
                        alert("Phone number already exist ...")
                    }else{
                        window.location.reload();
                    }
                }
            })
        });

        $("#back").click(function (event) {
            event.preventDefault();
            window.location.replace("/");
        })
    });
</script>
