package com.mytechjob.service;

import com.mytechjob.repository.model.entity.Address;
import com.mytechjob.repository.model.entity.Country;

import java.util.List;

public interface AddressService {
    void createOrUpdate(Address address);
    List<Address> getAll();
    void remove(Address address);
    Address findOneByCountry(Country country);
    Address findOneById(long id);
}
