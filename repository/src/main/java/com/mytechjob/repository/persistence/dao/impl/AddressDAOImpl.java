package com.mytechjob.repository.persistence.dao.impl;

import com.mytechjob.repository.model.entity.Address;
import com.mytechjob.repository.model.entity.Country;
import com.mytechjob.repository.persistence.dao.AddressDAO;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public class AddressDAOImpl implements AddressDAO{

    @Autowired
    HibernateTemplate hibernateTemplate;

    @Override
    public Address findOneById(long id) {
        return hibernateTemplate.get(Address.class,id);
    }

    @Override
    public Address findOneByCountry(Country country) {
        DetachedCriteria criteria = DetachedCriteria.forClass(Address.class).add(Restrictions.eq("country",country));
        List<Address> result = (List<Address>) hibernateTemplate.findByCriteria(criteria);
        return result.size()>0?result.get(0):null;
    }

    @Override
    @Transactional
    public void createOrUpdate(Address address) {
        hibernateTemplate.saveOrUpdate(address);
    }

    @Override
    @Transactional
    public void remove(Address address) {
        hibernateTemplate.delete(address);
    }

    @Override
    public List<Address> getAll() {
        return (List<Address>) hibernateTemplate.find("from Address");
    }
}
