package com.mytechjob.repository.persistence.dao;

import com.mytechjob.repository.model.entity.PhoneNumber;

import java.util.List;

public interface PhoneNumberDAO {
    PhoneNumber findOneById(long id);
    void createOrUpdate(PhoneNumber phoneNumber);
    void remove(PhoneNumber phoneNumber);
    List<PhoneNumber> getAll();
    boolean checkForExist(PhoneNumber number);
}
