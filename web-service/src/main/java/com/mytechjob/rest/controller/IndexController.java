package com.mytechjob.rest.controller;

import com.mytechjob.repository.model.entity.Address;
import com.mytechjob.repository.model.entity.Country;
import com.mytechjob.repository.model.entity.PhoneNumber;
import com.mytechjob.rest.RestConstants;
import com.mytechjob.rest.dto.PhoneNumberDTO;
import com.mytechjob.service.AddressService;
import com.mytechjob.service.PhoneNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


@Controller(value = RestConstants.BASIC_URL)
class IndexController{

    @Autowired
    AddressService addressService;
    @Autowired
    PhoneNumberService phoneNumberService;

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public ModelAndView getStartPage(){
        ModelAndView model = new ModelAndView("start_page");
        model.addObject("addresses",addressService.getAll());
        model.addObject("countries", Country.values());
        return model;
    }

    @RequestMapping(value = "/address/{id}",method = RequestMethod.GET)
    public ModelAndView getAddressInfoPage(@PathVariable("id") long id){
        ModelAndView model = new ModelAndView("address_info");
        Address address = addressService.findOneById(id);
        if(address == null){
            return new ModelAndView("error");
        }
        model.addObject("country",address.getCountry());
        model.addObject("numbers", address.getNumbers());
        model.addObject("addressId", id);
        return model;
    }

    @RequestMapping(value = "/address",method = RequestMethod.POST)
    public ResponseEntity createAddress(@RequestBody Address incoming){
        Address address = addressService.findOneByCountry(incoming.getCountry());
        if(address != null){
            return new ResponseEntity("address already exist",HttpStatus.OK);
        }
        addressService.createOrUpdate(incoming);
        return new ResponseEntity(incoming, HttpStatus.OK);
    }

    @RequestMapping(value = "/number", method = RequestMethod.POST)
    public ResponseEntity addPhoneNumberToAddress(@RequestBody PhoneNumberDTO dto){
        Address address = addressService.findOneByCountry(dto.getCountry());
        PhoneNumber phoneNumber = phoneNumberService.validateAndCreatePhoneNumber(dto.getPhoneNumber(),dto.getCountry());
        if(phoneNumber == null){
            return new ResponseEntity("not valid number",HttpStatus.OK);
        }
        if(phoneNumberService.checkForExist(phoneNumber)){
            return new ResponseEntity("number already exist",HttpStatus.OK);
        }
        address.getNumbers().add(phoneNumber);
        addressService.createOrUpdate(address);
        return new ResponseEntity(address,HttpStatus.OK);
    }

    @RequestMapping(value = "/number", method = RequestMethod.GET)
    public ResponseEntity removePhoneNumberFromAddress(@RequestParam("address")long addressId,
                                                       @RequestParam("number")long numberId){
        Address address = addressService.findOneById(addressId);
        PhoneNumber number = phoneNumberService.findOneById(numberId);
        if(address == null || number == null){
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        }
        phoneNumberService.remove(number);
        return new ResponseEntity(HttpStatus.OK);
    }
}
