<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <div>
        <table border="1">
            <tr>
                <th>
                    ID
                </th>
                <th>
                    COUNTRY
                </th>
                <th>

                </th>
            </tr>
            <c:choose>
                <c:when test="${not empty addresses}">
                    <c:forEach var="entry" items="${addresses}">
                        <tr>
                            <td>
                                    ${entry.addressId}
                            </td>
                            <td>
                                    ${entry.country.countryName}
                            </td>
                            <td>
                                <button class="address_info" data-id="${entry.addressId}">Details</button>
                            </td>
                        </tr>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                        <tr>
                            <td colspan="3">
                                empty
                            </td>
                        </tr>
                </c:otherwise>
            </c:choose>
        </table>
    </div>
    <div>
        <button id="address_add">Add new address</button>
    </div>
    <div id="address_popup" style="display: none; background-color: white; padding: 10px">
        <div>
            <select id="country_name">
                <option value="default" disabled selected>Select country</option>
                <c:forEach items="${countries}" var="entry">
                    <option value="${entry}">${entry.countryName}</option>
                </c:forEach>
            </select>
        </div>
        <div>
            <button id="address_submit">Add address</button>
        </div>
    </div>
