package com.mytechjob.rest.dto;

import com.mytechjob.repository.model.entity.Country;

public class PhoneNumberDTO {
    private String phoneNumber;
    private Country country;

    public PhoneNumberDTO() {
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
