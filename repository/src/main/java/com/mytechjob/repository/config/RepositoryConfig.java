package com.mytechjob.repository.config;

import org.springframework.context.annotation.Bean;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

import javax.sql.DataSource;

public interface RepositoryConfig {

    @Bean
    LocalSessionFactoryBean sessionFactory();

    @Bean
    DataSource dataSource();

    @Bean
    HibernateTransactionManager transactionManager();
}
