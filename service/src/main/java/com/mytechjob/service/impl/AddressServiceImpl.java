package com.mytechjob.service.impl;

import com.mytechjob.repository.model.entity.Address;
import com.mytechjob.repository.model.entity.Country;
import com.mytechjob.repository.persistence.dao.AddressDAO;
import com.mytechjob.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService{

    @Autowired
    AddressDAO addressDAO;

    @Override
    public void createOrUpdate(Address address) {
        addressDAO.createOrUpdate(address);
    }

    @Override
    public List<Address> getAll() {
        return addressDAO.getAll();
    }

    @Override
    public void remove(Address address) {
        addressDAO.remove(address);
    }

    @Override
    public Address findOneByCountry(Country country) {
        return addressDAO.findOneByCountry(country);
    }

    @Override
    public Address findOneById(long id) {
        return addressDAO.findOneById(id);
    }
}
